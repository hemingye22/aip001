#include "pch.h"
#include <iostream>
#include <exception>
#include <string>
#include <cmath>

using namespace std;

void mysqrt(float m)
{
	throw logic_error("Error, m<0");
}

void hysteric()
{
	try
	{
		throw logic_error("Error");
	}
	catch (exception i)
	{
		cout << i.what() << endl;
	}
}

void f()
{
	hysteric();
}

int main()
{
	int n;

	cin >> n;

	try
	{
		if (n < 0)
			throw logic_error("Age cannot be negative");
	}
	catch (exception i)
	{
		cout << i.what() << endl;
	}

	int a, b;

	cin >> a;
	cin >> b;

	try
	{
		if (b == 0)
			throw logic_error("Error, divide on 0");
		cout << a / b << endl;
	}
	catch (exception i)
	{
		cout << i.what() << endl;
	}

	int x;

	cin >> x;

	try
	{
		if (x <= 0)
			throw logic_error("x < 0");
		cout << log(x) << endl;
	}
	catch (exception i)
	{
		cout << i.what() << endl;
	}

	float m;

	cin >> m;

	try
	{
		if (m < 0)
		   mysqrt(m);
		cout << sqrt(m) << endl;
	}
	catch (exception i)
	{
		cout << i.what() << endl;
	}

	f();

	return 0;
}
